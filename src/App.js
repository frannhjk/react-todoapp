import React, { useState, useEffect } from 'react';
import './App.css';


import Form from './components/Form';
import TodoList from './components/TodoList';

function App() {

  // StateHook: InputText es el valor y setInputText es la función para setearlo
  const [inputText, setInputText] = useState('');
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState('all');
  const [filteredTodos, setFilteredTodos] = useState([]);

  // UseEffect: Corre sólo una vez cuando se inicia la aplicación (, [])
  useEffect(() => {
    getLocalTodos();  
  }, []);

  // UseEffect: Cuando cambien los 'todos' y 'filter' (Input - Select)
  useEffect(() => {
    console.log("hola");
    saveLocalTodos();
    filterHandler();
  }, [todos, filter]);
  

  // Functions
  // Se ejecutara cada vez que se cambie algo (vease: Input submit new Todo o se selecciona un filtro en el Combo)
  // gracias al -UseEffect-
  const filterHandler = () => {
    switch(filter){
      case 'completed':
        setFilteredTodos(todos.filter(todo => todo.completed === true));
        break;

      case 'uncompleted':
        setFilteredTodos(todos.filter(todo => todo.completed === false));
        break;

      default: 
        setFilteredTodos(todos);
        break;
    }
  };

  // Save to LocalStorage
  const saveLocalTodos = () => {
    localStorage.setItem('todos', JSON.stringify(todos));
  };

  // Get LocalStorage 
  const getLocalTodos = () => {
    if (localStorage.getItem('todos') === null){
      localStorage.setItem('todos', JSON.stringify([]));
    }
    else{
      var todoFromLocalStorage = JSON.parse(localStorage.getItem('todos'));
      setTodos(todoFromLocalStorage);
    }
  };

  return (
    <div className="App">
        <header>
        <h1>Fran TODOs</h1>
        </header>
        <div>
          <Form 
            todos={todos} 
            setTodos={setTodos} 
            setInputText={setInputText} 
            inputText={inputText}
            setFilter={setFilter}
          />
          <TodoList 
            setTodos={setTodos} 
            todos={todos}
            filteredTodos={filteredTodos}
          />
        </div>
    </div>
  );
}

export default App;
