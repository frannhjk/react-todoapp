import React from 'react';


const Form = (props) => {

  // Events
  const inputTextHandler = (e) => {
    props.setInputText(e.target.value);
  };

  const submitTodoHandler = (e) => {
    console.log("Click Submit");
    e.preventDefault(); // No Actualizar
    props.setTodos([
      // Si ya hay todos que los ponga aca, sino que defina nuevos
      ...props.todos, 
      { id: Math.random() * 1000, text: props.inputText, completed: false }
    ]);
    props.setInputText('');
  };

  const filterHandler = (e) => {
    props.setFilter(e.target.value);
    console.log(e.target.value);
  };

  return (
    <form>
      <input value={props.inputText} onChange={inputTextHandler} type="text" className="todo-input" />
      <button onClick={submitTodoHandler} className="todo-button" type="submit">
        <i className="fas fa-plus-square"></i>
      </button>
      <div className="select">
        <select name="todos" className="filter-todo" onChange={filterHandler}>
          <option value="all">All</option>
          <option value="completed">Completed</option>
          <option value="uncompleted">Uncompleted</option>
        </select>
      </div>
    </form>
    );
}

export default Form;